#!/bin/bash
set -eu -o pipefail

available() {
  command -v "$1" >/dev/null 2>&1
}

# Checks will be performed before downloading
echo "Performing checks..."

# Make sure we have wget or curl and xz
if available wget; then
  DL="wget"
  DL_SILENT="wget -qO-"
elif available curl; then
  DL="curl -LO"
  DL_SILENT="curl -s"
else
  echo "This program requires wget or curl. Please install either of them."
  exit 1
fi

# Make sure we have unxz
if available unxz; then
  found=true
else
  echo "This program requires unxz. Please install it."
  exit 1
fi

# Use the architecture of the current machine or whatever the user has set
# externally
ARCH="${ARCH:-$(uname -m)}"
case "$ARCH" in
  x86_64) REAPER_ARCH="x86_64" ;;
    i?86) REAPER_ARCH="i686" ;;
 aarch64) REAPER_ARCH="aarch64" ;;
    arm*) REAPER_ARCH="armv7l" ;;
       *) echo "The architecture $ARCH is not supported." >&2 ; exit 1 ;;
esac

# Set Output dir
REAPER_INSTALL_DIR="${REAPER_INSTALL_DIR:-/opt/reaper}"

# Set temp dir
TMP="${TMP:-/tmp}"

# Set staging dir
STAGINGDIR="${STAGINGDIR:-/tmp/reaper-staging}"

# If the staging directory is already present from the past, clear it down and
# re-create it.
if [ -d "$STAGINGDIR" ]; then
  rm -fr "$STAGINGDIR"
fi
mkdir -p "$STAGINGDIR"

# Work out the latest Reaper version
# Expected output (sample): v6.67
$DL_SILENT "https://reaper.fm/download.php" > "$STAGINGDIR/download.php"
REAPER_VERSION="$(cat "$STAGINGDIR/download.php" | awk '{print $2}' "$STAGINGDIR/download.php" | head -n160 | tail -n1)"

echo "Reaper Version: ${REAPER_VERSION}"

# Get major version
REAPER_MAJVERSION=$(echo "${REAPER_VERSION}" | cut -d'.' -f1 | tr -d 'v')

# Expected output (sample): 667 (desired result, without v nor .)
REAPER_VERSION_PARSED=${REAPER_VERSION//[v.]/}

###

# Error out if $CDMVERISON is unset, e.g. because previous command failed
if [ -z "$REAPER_INSTALL_DIR" ]; then
  echo "Could not work out the latest version; exiting" >&2
  exit 1
fi

# Don't start repackaging if the same version is already installed
if [ -e "$REAPER_INSTALL_DIR/reaper-version.txt" ] && [ "$REAPER_VERSION" = "$(cat $REAPER_INSTALL_DIR/reaper-version.txt)" ]; then
  echo "The latest Reaper ($REAPER_VERSION) is already installed"
  exit 0
fi

# Make and switch to the staging directory
cd "$STAGINGDIR"

# Now get the latest Reaper zip for the users architecture
echo "Downloading Reaper version ${REAPER_VERSION_PARSED}..."
REAPER_PACKAGE="reaper${REAPER_VERSION_PARSED}_linux_${REAPER_ARCH}"
$DL "https://www.reaper.fm/files/${REAPER_MAJVERSION}.x/${REAPER_PACKAGE}.tar.xz"

# Extract the contents of Reaper package
echo "Extracting Reaper $REAPER_VERSION ($REAPER_ARCH)..."
tar -xf "${REAPER_PACKAGE}.tar.xz"
echo "Done!"

# Copy files into place (we should be superuser, as we will execute this script in postinstall)
mkdir -p $REAPER_INSTALL_DIR
cp -r reaper_linux_$REAPER_ARCH/* $REAPER_INSTALL_DIR

# Desktop integration
cd $REAPER_INSTALL_DIR
bash "install-reaper.sh" --integrate-sys-desktop

# Install txt file indicating the installed version.
echo $REAPER_VERSION > "$REAPER_INSTALL_DIR/reaper-version.txt"

# Tell the user we are done
echo "Reaper ($REAPER_VERSION) installed into $REAPER_INSTALL_DIR"
