# Reaper Downloader

This package aims to provide a Debian installer for [Reaper software](https://www.reaper.fm/).

Since the software does not have a suitable redistributing License (an agreement has to be made beforehand), 
this meta-package downloads the program from their official web page and installs it to the user without any extra scripts involved.

* A reliable Internet connection is required to perform the download.
